#include <GLFW/glfw3.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#include <math.h>
#include <iostream>

#include "ball.h"
#include "tools.h"

void Ball::draw()
{
	glColor3f(this->color.r, this->color.g, this->color.b);

	glPushMatrix();
		glTranslatef(this->position.x, this->position.y, this->position.z);
		glScalef(this->rad.x, this->rad.y, this->rad.z);
		drawSphere();
	glPopMatrix();
}

void Ball::move()
{
	this->position = pointPlusVector(this->position, this->direction);
}

bool Ball::isInFront(Point3D coord)
{
	return this->position.z < coord.z;
}

void Ball::reset(Point3D position, Vector3D direction)
{
	this->position = position;
	this->direction = direction;
}

void Ball::drawShadow(float viewSize)
{
	glColor3f(0., 0., 0.);
	glPushMatrix();
		// 0.1 == border
		glTranslatef(this->position.x, -(viewSize / 10. - 0.1), this->position.z);
		glScalef(this->rad.x, this->rad.y / 2., this->rad.z);
		drawCircle();
	glPopMatrix();
}

void Ball::wait(GLFWwindow *window, Point3D position, Vector3D direction)
{
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
	{
		this->stick = false;
	}
	else
	{
		this->reset(position, direction);
	}
}

bool Ball::inRacket(Point3D racketPosition, float racketScale)
{
	if (isBetween(this->position.x, this->rad.x, racketPosition.x - racketScale / 2, racketPosition.x + racketScale / 2)
	 && isBetween(this->position.y, this->rad.y, racketPosition.y - racketScale / 2, racketPosition.y + racketScale / 2))
	{
		// Upper-left corner
		if (isBetween(this->position.x, this->rad.x, racketPosition.x - racketScale / 6, racketPosition.x)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y, racketPosition.y + racketScale / 6))
		{
			this->positionWithinRacket = TOP_LEFT_1;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x - racketScale / 6 * 2, racketPosition.x)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y, racketPosition.y + racketScale / 6 * 2))
		{
			this->positionWithinRacket = TOP_LEFT_2;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x - racketScale / 2, racketPosition.x)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y, racketPosition.y + racketScale / 2))
		{
			this->positionWithinRacket = TOP_LEFT_3;
		}

		// Upper-right corner
		if (isBetween(this->position.x, this->rad.x, racketPosition.x, racketPosition.x + racketScale / 6)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y, racketPosition.y + racketScale / 6))
		{
			this->positionWithinRacket = TOP_RIGHT_1;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x, racketPosition.x + racketScale / 6 * 2)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y, racketPosition.y + racketScale / 6 * 2))
		{
			this->positionWithinRacket = TOP_RIGHT_2;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x, racketPosition.x + racketScale / 2)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y, racketPosition.y + racketScale / 2))
		{
			this->positionWithinRacket = TOP_RIGHT_3;
		}

		// Lower-left corner
		if (isBetween(this->position.x, this->rad.x, racketPosition.x - racketScale / 6, racketPosition.x)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y - racketScale / 6, racketPosition.y))
		{
			this->positionWithinRacket = BOTTOM_LEFT_1;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x - racketScale / 6 * 2, racketPosition.x)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y - racketScale / 6 * 2, racketPosition.y))
		{
			this->positionWithinRacket = BOTTOM_LEFT_2;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x - racketScale / 2, racketPosition.x)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y - racketScale / 2, racketPosition.y))
		{
			this->positionWithinRacket = BOTTOM_LEFT_3;
		}

		// Lower-right corner
		if (isBetween(this->position.x, this->rad.x, racketPosition.x, racketPosition.x + racketScale / 6)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y - racketScale / 6, racketPosition.y))
		{
			this->positionWithinRacket = BOTTOM_RIGHT_1;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x, racketPosition.x + racketScale / 6 * 2)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y - racketScale / 6 * 2, racketPosition.y))
		{
			this->positionWithinRacket = BOTTOM_RIGHT_2;
		}
		if (isBetween(this->position.x, this->rad.x, racketPosition.x, racketPosition.x + racketScale / 2)
		 && isBetween(this->position.y, this->rad.y, racketPosition.y - racketScale / 2, racketPosition.y))
		{
			this->positionWithinRacket = BOTTOM_RIGHT_3;
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool Ball::inWall(Vector3D scene)
{
	if (isBetween(this->position.y, -this->rad.y, -scene.y, scene.y))
	{	if(this->position.x - this->rad.x <= -scene.x)
			this->positionWhichWall = LEFT_WALL;
		else if(this->position.x + this->rad.x >= scene.x)
			this->positionWhichWall = RIGHT_WALL;
		else 
			return false;
		return true;
	}

	if (isBetween(this->position.x, -this->rad.x, -scene.x, scene.x))
	{
		if(this->position.y + this->rad.y >= scene.y)
			this->positionWhichWall = TOP_WALL;
		else if(this->position.y - this->rad.y <= -scene.y)
			this->positionWhichWall = BOTTOM_WALL;
		else
			return false;
		return true;
	}

	return false;
}

void Ball::newDirection(Vector3D norm, float coef)
{
	this->direction.x = this->direction.x - norm.x * 2 * dot(this->direction, norm) + coef;
	this->direction.y = this->direction.y - norm.y * 2 * dot(this->direction, norm) + coef;
	this->direction.z = this->direction.z - norm.z * 2 * dot(this->direction, norm);
}

void Ball::newDirectionRacket(Vector3D racketNorm)
{
	// left side of racket
	if (this->positionWithinRacket == TOP_LEFT_1 || this->positionWithinRacket == BOTTOM_LEFT_1)
	{
		this->newDirection(racketNorm, -0.001);
	}
	if (this->positionWithinRacket == TOP_LEFT_2 || this->positionWithinRacket == BOTTOM_LEFT_2)
	{
		this->newDirection(racketNorm, -0.006);
	}
	if (this->positionWithinRacket == TOP_LEFT_3 || this->positionWithinRacket == BOTTOM_LEFT_3)
	{
		this->newDirection(racketNorm, -0.012);
	}

	// right side of racket
	if (this->positionWithinRacket == TOP_RIGHT_1 || this->positionWithinRacket == BOTTOM_RIGHT_1)
	{
		this->newDirection(racketNorm, 0.001);
	}
	if (this->positionWithinRacket == TOP_RIGHT_2 || this->positionWithinRacket == BOTTOM_RIGHT_2)
	{
		this->newDirection(racketNorm, 0.006);
	}
	if (this->positionWithinRacket == TOP_RIGHT_3 || this->positionWithinRacket == BOTTOM_RIGHT_3)
	{
		this->newDirection(racketNorm, 0.012);
	}
}

void Ball::newDirectionWall()
{
	Vector3D wallNorm;
	
	// left / right walls
	if (this->positionWhichWall == LEFT_WALL)
	{
		wallNorm = createVector(1., 0., 0.);
		this->newDirection(wallNorm, 0);
	}
	if (this->positionWhichWall == RIGHT_WALL)
	{
		wallNorm = createVector(-1., 0., 0.);
		this->newDirection(wallNorm, 0);
	}

	// top / bottom walls
	if (this->positionWhichWall == TOP_WALL)
	{
		wallNorm = createVector(0., -1., 0.);
		this->newDirection(wallNorm, 0);
	}
	if (this->positionWhichWall == BOTTOM_WALL)
	{
		wallNorm = createVector(0., 1., 0.);
		this->newDirection(wallNorm, 0);
	}
}
