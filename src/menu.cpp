#include "menu.h"
#include "stb_image.h"
#include "texture.h"
#include "tools.h"

#include "geometry.h"

void displayMenu(GLuint image, float depth){
    float mid = GL_VIEW_SIZE / 10.;
    glPushMatrix();
        glTranslatef(0, 0, depth);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, image);
			glBegin(GL_QUADS);

            glTexCoord2f(0, 0);
            glVertex2f(-mid * 2, mid);

            glTexCoord2f(1, 0);
            glVertex2f(mid * 2, mid);
            
            glTexCoord2f(1, 1);
            glVertex2f(mid * 2, -mid);

            glTexCoord2f(0, 1);
            glVertex2f(-mid * 2, -mid);

            glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}