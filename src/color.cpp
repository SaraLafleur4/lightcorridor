#include "color.h"

ColorRGB createColor(float r, float g, float b)
{
    ColorRGB newColor;
    newColor.r = r;
    newColor.g = g;
    newColor.b = b;

    return newColor;
}