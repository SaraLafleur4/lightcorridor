#include <GLFW/glfw3.h>
#include "racket.h"
#include "tools.h"

void Racket::draw()
{
	glColor3f(this->color.r, this->color.g, this->color.b);

	glPushMatrix();
		glTranslatef(this->position.x, this->position.y, this->position.z);
		glScalef(this->scale, this->scale, 0.);
		drawSquare();
	glPopMatrix();
}

void Racket::moveForward(int iterate)
{
	this->position = pointPlusVector(this->position, multVector(this->direction, iterate));
}

void Racket::resetColor(){
	this->color = this->defaultColor;
}