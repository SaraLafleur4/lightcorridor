#include "tools.h"

/* Convert degree to radians */
float toRad(float deg)
{
    return deg * M_PI / 180.0f;
}

/* Axes visualization */
void drawFrame()
{
    glBegin(GL_LINES);

    glVertex3f(0., 0., 0.);
    glVertex3f(10., 0., 0.);

    glVertex3f(0., 0., 0.);
    glVertex3f(0., 10., 0.);

    glVertex3f(0., 0., 0.);
    glVertex3f(0., 0., 10.);

    glEnd();
}

/* Error handling function */
void onError(int error, const char *description)
{
    fprintf(stderr, "GLFW Error: %s\n", description);
}

void onWindowResized(GLFWwindow *window, int width, int height)
{
    aspectRatio = width / (float)height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, aspectRatio, Z_NEAR, Z_FAR);
    glMatrixMode(GL_MODELVIEW);
}

void onKey(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        switch (key)
        {
        case GLFW_KEY_A:
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GLFW_TRUE);
            break;
        default:
            // fprintf(stdout, "This key isn't initialised (%d)\n", key);
			break;
		}
    }
}

bool isBetween(float compare, float border, float smallest, float biggest){
	return smallest - border <= compare  && compare  <= biggest + border;
}

// CANONIC OBJECT

void drawCone(){
	glBegin(GL_TRIANGLE_FAN);
		glVertex3f(0.0,0.0,1.0);
		float step_rad = 2* M_PI / (float)NB_SEG_CIRCLE;
		for(int i = 0; i <= NB_SEG_CIRCLE; i++) {
			glVertex3f(cos(i*step_rad),sin(i*step_rad),0.0f);
		}
	glEnd();
}

void drawHeart(){
	glPushMatrix();
		glScalef(0.1, 0.1, 0.);
		glTranslatef(-0.75, 0.5, 0.);
		
		drawSphere();

		glTranslatef(1.5, 0., 0.);
		drawSphere();

		glTranslatef(-0.75, -0.5, 0.);
		glScalef(1.62, 1.62, 0.);
		glRotatef(90., 0., 1., 0.);
		glRotatef(90., 1., 0., 0.);
		drawCone();
	glPopMatrix();
}

void drawCircle()
{
	glBegin(GL_TRIANGLE_FAN);
	glVertex3f(0., 0., 0.);
	float step_rad = 2 * M_PI / (float)NB_SEG_CIRCLE;
	for (int i = 0; i <= NB_SEG_CIRCLE; i++)
	{
		glVertex3f(cos(i * step_rad), sin(i * step_rad), 0.0f);
	}
	glEnd();
}

void drawSphere()
{
	gluSphere(gluNewQuadric(), 1.0, NB_SEG_CIRCLE, NB_SEG_CIRCLE);
}

void drawSquare()
{
	float mid = 1 / 2.;
	glBegin(GL_LINES);
	for (int i = -1; i <= 1; i += 2)
	{
		glVertex3f(-mid * i, mid * i, 0.);
		glVertex3f(mid * i, mid * i, 0.);

		glVertex3f(mid * i, mid * i, 0.);
		glVertex3f(mid * i, -mid * i, 0.);
	}
	glEnd();
}