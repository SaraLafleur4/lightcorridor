#include <GLFW/glfw3.h>

#include "obstacle.h"
#include "tools.h"

//// Individual Panels -----------------------

void drawPanelRight(Vector3D scene, GLuint image)
{
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(0.5, scene.y);

	glTexCoord2f(1, 0);
	glVertex2f(scene.x, scene.y);
	
	glTexCoord2f(1, 1);
	glVertex2f(scene.x, -scene.y);

	glTexCoord2f(0, 1);
	glVertex2f(0.5, -scene.y);

	glEnd();
}

void drawPanelCenter(Vector3D scene, GLuint image)
{
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(-0.5, scene.y);

	glTexCoord2f(1, 0);
	glVertex2f(0.5, scene.y);
	
	glTexCoord2f(1, 1);
	glVertex2f(0.5, -scene.y);

	glTexCoord2f(0, 1);
	glVertex2f(-0.5, -scene.y);
	
	glEnd();
}

void drawPanelLeft(Vector3D scene, GLuint image)
{
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(-scene.x, scene.y);

	glTexCoord2f(1, 0);
	glVertex2f(-0.5, scene.y);
	
	glTexCoord2f(1, 1);
	glVertex2f(-0.5, -scene.y);

	glTexCoord2f(0, 1);
	glVertex2f(-scene.x, -scene.y);
	
	glEnd();
}

void drawPanelTop(Vector3D scene, GLuint image)
{
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(-scene.x, scene.y);

	glTexCoord2f(1, 0);
	glVertex2f(scene.x, scene.y);
	
	glTexCoord2f(1, 1);
	glVertex2f(scene.x, 0.2);

	glTexCoord2f(0, 1);
	glVertex2f(-scene.x, 0.2);
	
	glEnd();
}

void drawPanelBottom(Vector3D scene, GLuint image)
{
	glBegin(GL_QUADS);

	glTexCoord2f(0, 0);
	glVertex2f(-scene.x, -0.2);

	glTexCoord2f(1, 0);
	glVertex2f(scene.x, -0.2);
	
	glTexCoord2f(1, 1);
	glVertex2f(scene.x, -scene.y);

	glTexCoord2f(0, 1);
	glVertex2f(-scene.x, -scene.y);
	
	glEnd();
}

void Panel::drawPanel(Vector3D scene, GLuint image)
{
	switch (this->id)
	{
	case RIGHT_PANEL:
		drawPanelRight(scene, image);
		break;
	case CENTER_PANEL:
		drawPanelCenter(scene, image);
		break;
	case LEFT_PANEL:
		drawPanelLeft(scene, image);
		break;
	case TOP_PANEL:
		drawPanelTop(scene, image);
		break;
	case BOTTOM_PANEL:
		drawPanelBottom(scene, image);
		break;

	default:
		drawPanelCenter(scene, image);
	}
}

void Panel::draw(float position, float variation, Vector3D scene, GLuint image)
{
	// single panel
	int depth = 0;
	float gap = -this->depth + position;
	if (gap > variation)
	{
		depth++;
		if (gap > variation * 2)
			depth++;
	}

	glPushMatrix();
		glColor3f(this->color.r - 0.2 * depth, this->color.g - 0.2 * depth, this->color.b - 0.2 * depth);
		glTranslatef(0., 0., this->depth);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, image);
			this->drawPanel(scene, image);
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

bool Panel::inPanel(Point3D point, float border, Vector3D scene)
{
	switch (this->id)
	{
	case RIGHT_PANEL:
		return isBetween(point.x, border, 0.5, scene.x)
		 && isBetween(point.y, border, -scene.y, scene.y);
	case CENTER_PANEL:
		return isBetween(point.x, border, -0.5, 0.5)
		 && isBetween(point.y, border, -scene.y, scene.y);
	case LEFT_PANEL:
		return isBetween(point.x, border, -scene.x, -0.5)
		 && isBetween(point.y, border, -scene.y, scene.y);
	case TOP_PANEL:
		return isBetween(point.x, border, -scene.x, scene.x)
		 && isBetween(point.y, border, 0.2, scene.y);
	case BOTTOM_PANEL:
		return isBetween(point.x, border, -scene.x, scene.x)
		 && isBetween(point.y, border, -scene.y, -0.2);
	default:
		return false;
	}
}


//// List of Panels --------------------------

void Panels::draw(float position, float variation, Vector3D scene, GLuint image)
{
	for (int i = 0; i < this->panels.size(); i++)
	{
		this->panels[i].draw(position, variation, scene, image);
	}
}
