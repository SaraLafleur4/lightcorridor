#include "bonus.h"
#include "tools.h"

// Bonus ------------------------
bool Bonus::isHit(coords3D position, float border){
    bool result = isBetween(position.x, border, this->position.x - this->minSize.x, this->position.x + this->maxSize.x) 
        && isBetween(position.y, border, this->position.y - this->minSize.y, this->position.y + this->maxSize.y)
        && isBetween(position.z, border / 10., this->position.z, this->position.z);
    return !this->applied && result;
}

void Bonus::draw(){
    glPushMatrix();
    glTranslatef(this->position.x, this->position.y, this->position.z);
    glRotatef(this->angle, 0., 1., 0.);
    this->angle += rand() % 10 / 10.;

    switch (this->id)
    {
    case LIFE:
        glColor3f(1., 0., 0.);
        drawHeart();
        break;
    case GLUE:
        glColor3f(0, 0, 1);
        drawHeart();
        break;
    default:
        break;
    }
    glPopMatrix();
}

bool Bonus::stillActive(){
    return this->applied && this->time > 0.;
}
// LstBonus ------------------------

void LstBonus::draw(){
    for(unsigned int i = 0; i < this->bonus.size(); i++){
        if(!this->bonus.at(i).applied)
            this->bonus.at(i).draw();
    }
}

int LstBonus::isHit(coords3D position, float border){
    for(unsigned int i = 0; i < this->bonus.size(); i++){
        if(this->bonus.at(i).isHit(position, border)){
            return i;
        }
    }
    return -1;
}

void LstBonus::desactivate(int index){
    this->bonus.at(index).applied = true;
}

bool LstBonus::useBonus(idBonus id){
    float sumTime = 0.;
    for(unsigned int i = 0; i < this->bonus.size(); i++){
        Bonus bonus = this->bonus.at(i);
        if(bonus.id == id && bonus.stillActive()){
            if(sumTime == 0.)
                this->bonus.at(i).time -= 0.1;
            sumTime += bonus.time;
        }
    }
    return sumTime > 0.;
}

void LstBonus::resetActive(){
    for(unsigned int i = 0; i < this->bonus.size(); i++){
        Bonus bonus = this->bonus.at(i);
        if(bonus.stillActive()){            
            this->bonus.at(i).time = 0;
        }
    }
}