#define GLFW_INCLUDE_NONE

#include <GLFW/glfw3.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tools.h"
#include "scene.h"
#include "texture.h"
#include "stb_image.h"
#include "menu.h"

int main(int argc, char const *argv[])
{
    unsigned char *recup_menu = NULL;
    unsigned char *recup_texture = NULL;
    unsigned char *recup_win = NULL;
    unsigned char *recup_lose = NULL;

    Scene *scene = new Scene();
    bool init = false;
    /* GLFW initialisation */
    GLFWwindow *window;
    if (!glfwInit())
        return -1;

    /* Callback to a function if an error is rised by GLFW */
    glfwSetErrorCallback(onError);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, NULL, NULL);
    if (!window)
    {
        // If no context created : exit !
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    /* Callback functions */
    glfwSetWindowSizeCallback(window, onWindowResized);
    glfwSetKeyCallback(window, onKey);

    /* Update window on resize */
    onWindowResized(window, WINDOW_WIDTH, WINDOW_HEIGHT);

    /* Drawing parameters */
    glPointSize(5.0);
    glEnable(GL_DEPTH_TEST);

    /* Load image and get texture reference */
    GLuint menu = loadImage("doc/start_menu.jpg", recup_menu);
	GLuint brick_texture = loadImage("doc/white_brick_wall.jpg", recup_texture);
    
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Get time (in second) at loop beginning */
        double startTime = glfwGetTime();

        /* Cleaning buffers and setting Matrix Mode */
        glClearColor(0., 0., -0.3, 51 / 255.);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        /* Initial scenery setup */
        scene->camera->set();

        /* Scene rendering */
        scene->draw(brick_texture);
        
        if(!init){
            displayMenu(menu, scene->racket->position.z - 1.);
        }

        if (scene->finished())
        {
            if(scene->life > 0){
                GLuint end_win = loadImage("doc/end_win.jpg", recup_win);
                displayMenu(end_win, scene->racket->position.z - 1.);
                glDeleteTextures(1, &end_win);
                stbi_image_free(recup_win);
            }
            else{
                GLuint end_lose = loadImage("doc/end_lose.jpg", recup_lose);
                displayMenu(end_lose, scene->racket->position.z - 1.);
                glDeleteTextures(1, &end_lose);
                stbi_image_free(recup_lose);
            }
                
        }
        
        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();

        /* Elapsed time computation from loop begining */
        double elapsedTime = glfwGetTime() - startTime;
        /* If to few time is spend vs our wanted FPS, we wait */
        while (elapsedTime < FRAMERATE_IN_SECONDS)
        {
            glfwWaitEventsTimeout(FRAMERATE_IN_SECONDS - elapsedTime);
            elapsedTime = glfwGetTime() - startTime;
        }

        /* Animate scenery */
        scene->moveRacket(window);

        if (scene->ball->stick
            && isBetween(scene->ball->position.z, scene->ball->rad.x, 
                scene->racket->position.z - scene->ball->rad.z, scene->racket->position.z)
        ){
            scene->ball->wait(window, scene->racket->position, scene->ball->initialDirection);
        }
        else
        {
            init = true; // To make disapear the menu
            scene->ball->move();

            scene->getBonus();

            if (!scene->ball->isInFront(scene->racket->position))
            {
                scene->ball->stick = true;
                scene->ball->reset(scene->racket->position, scene->ball->initialDirection);
                scene->bonus->resetActive();
            }

            if (!scene->racketIsBlocked() && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
            {
                scene->racket->moveForward();
                scene->camera->move(scene->racket->direction);
            }

            if (scene->collisionWithObstacle())
            {
                scene->ball->newDirection(scene->obstacles->panels[0].norm, 0);
            }
        }

        if (scene->collisionWithRacket())
        {
            scene->ball->newDirectionRacket(scene->racket->norm);
        }
        if (scene->collisionWithWall())
        {
            scene->ball->newDirectionWall();
        }

    }

    glDeleteTextures(1, &brick_texture);
    stbi_image_free(recup_texture);
    glDeleteTextures(1, &menu);
    stbi_image_free(recup_menu);
    
    glfwTerminate();
    return 0;
}
