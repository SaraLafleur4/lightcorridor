#include <iostream>
#define STB_IMAGE_IMPLEMENTATION
#include "texture.h"
#include "stb_image.h"

GLuint loadImage(char *imageName, unsigned char *recup_image)
{
	GLsizei n = 1;
	int textureWidth, textureHeight, comp;
	recup_image = stbi_load(imageName, &textureWidth, &textureHeight, &comp, 0);

	if (recup_image == NULL)
	{
		std::cout << "Error. Image didn't load." << '\n';
		exit(1);
	}

	GLuint image;
	glGenTextures(n, &image);

	glBindTexture(GL_TEXTURE_2D, image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // GL_RGBA pour les .png
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, recup_image);

	glBindTexture(GL_TEXTURE_2D, 0);

	return image;
}
