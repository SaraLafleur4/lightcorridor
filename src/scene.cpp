#include <GLFW/glfw3.h>
#include <iostream>
using namespace std;

#include "scene.h"
#include "tools.h"

void drawLeftWall(float x, float y, float z)
{
	glBegin(GL_TRIANGLE_FAN);

	glVertex3f(-x, -y, 0.);
	glVertex3f(-x, -y, -z);
	glVertex3f(-x, y, -z);
	glVertex3f(-x, y, 0.);

	glEnd();
}

void drawRightWall(float x, float y, float z)
{
	glBegin(GL_TRIANGLE_FAN);

	glVertex3f(x, -y, 0.);
	glVertex3f(x, -y, -z);
	glVertex3f(x, y, -z);
	glVertex3f(x, y, 0.);

	glEnd();
}

void drawTopWall(float x, float y, float z)
{
	glBegin(GL_TRIANGLE_FAN);

	glVertex3f(-x, y, 0.);
	glVertex3f(-x, y, -z);
	glVertex3f(x, y, -z);
	glVertex3f(x, y, 0.);

	glEnd();
}

void drawBottomWall(float x, float y, float z)
{
	glBegin(GL_TRIANGLE_FAN);

	glVertex3f(-x, -y, 0.);
	glVertex3f(-x, -y, -z);
	glVertex3f(x, -y, -z);
	glVertex3f(x, -y, 0.);

	glEnd();
}

/* Scene setup -> 4 walls of the corridor */
void scene::drawWall(Point3D position)
{
	// draw walls
	glPushMatrix();
		glTranslatef(0., 0., position.z);
		for(int i = 0; i < 3; i++){
			glPushMatrix();
				glColor3f(this->color.r - 0.1 * i, this->color.g - 0.1 * i, this->color.b - 0.1 * i);
				glTranslatef(0., 0., - this->wallDimensions.z / 3 * i);
				drawLeftWall(this->wallDimensions.x, this->wallDimensions.y, this->wallDimensions.z / 3);
				drawRightWall(this->wallDimensions.x, this->wallDimensions.y, this->wallDimensions.z / 3);
				
				glColor3f(this->color.r - 0.2 * (i + 1), this->color.g - 0.2 * (i + 1), this->color.b - 0.2 * (i + 1));
				drawTopWall(this->wallDimensions.x, this->wallDimensions.y, this->wallDimensions.z / 3);
				drawBottomWall(this->wallDimensions.x, this->wallDimensions.y, this->wallDimensions.z / 3);
			glPopMatrix();
		}
	glPopMatrix();
}

void Scene::draw(GLuint texture){
	drawLife();
    racket->draw();
    ball->draw();
    ball->drawShadow(GL_VIEW_SIZE);
    drawWall(racket->position);
    bonus->draw();
    // Elements with textures have to be drawn last
    obstacles->draw(racket->position.z, wallDimensions.z / 3, wallDimensions, texture);
}

bool scene::gainLife()
{
	(this->life)++;
	return true;
}

bool scene::lostLife()
{
	return (this->life)-- > 0;
}

void scene::moveRacket(GLFWwindow *window)
{
	double X_screen, Y_screen;
	glfwGetCursorPos(window, &X_screen, &Y_screen);

	float x_scene = this->wallDimensions.x + 0.07;
	float y_scene = this->wallDimensions.y + 0.17;

	float Ax = (GL_VIEW_SIZE * (x_scene / GL_VIEW_SIZE)) * 2 / WINDOW_WIDTH;
	float Bx = -GL_VIEW_SIZE * (x_scene / GL_VIEW_SIZE);
	float Ay = -y_scene * 2 / WINDOW_HEIGHT;
	float By = y_scene;

	float X_virtual;
	float Y_virtual;

	if (aspectRatio > 1)
	{
		X_virtual = Ax * aspectRatio * X_screen + Bx * aspectRatio;
		Y_virtual = Ay * Y_screen + By;
	}
	else
	{
		X_virtual = Ax * X_screen + Bx;
		Y_virtual = Ay * aspectRatio * Y_screen + By * aspectRatio;
	}

	X_virtual = min(max(X_virtual, -this->wallDimensions.x + this->border), this->wallDimensions.x - this->border);
	Y_virtual = min(max(Y_virtual, -this->wallDimensions.y + this->border), this->wallDimensions.y - this->border);

	this->racket->position.x = X_virtual;
	this->racket->position.y = Y_virtual;
}

bool Scene::racketIsBlocked()
{
	Point3D simulation = pointPlusVector(this->racket->position, this->racket->direction);

	for (int i = 0; i < this->obstacles->panels.size(); i++)
	{
		Panel panel = this->obstacles->panels.at(i);

		if (isBetween(panel.depth, 0, simulation.z, this->racket->position.z)
		 && panel.inPanel(this->racket->position, this->racket->scale / 2., this->wallDimensions))
		{
			return true;
		}
	}
	return false;
}

bool Scene::collisionWithObstacle()
{
	Point3D simulation = pointPlusVector(this->ball->position, this->ball->direction);

	for (int i = 0; i < this->obstacles->panels.size(); i++)
	{
		Panel panel = this->obstacles->panels.at(i);

		if (isBetween(panel.depth, 0, simulation.z, this->ball->position.z)
		 && panel.inPanel(this->ball->position, this->ball->rad.x, this->wallDimensions))
		{
			return true;
		}
	}
	return false;
}

bool Scene::collisionWithRacket()
{
	return this->ball->direction.z > 0 
		&& isBetween(this->racket->position.z, this->ball->rad.z, 
			this->ball->position.z, this->ball->position.z) 
		&& this->ball->inRacket(this->racket->position, this->racket->scale)
	;
}

bool Scene::collisionWithWall()
{
	return isBetween(this->ball->position.z, -this->ball->rad.z, -this->lenght, this->racket->position.z) 
		&& this->ball->inWall(this->wallDimensions)
	;
}

bool Scene::finished(){
	return - this->racket->position.z > this->lenght || this->life <= 0;
}

void scene::drawLife(){
	for(int i = 0; i < this->life; i++){
		glPushMatrix();
		glTranslatef(- GL_VIEW_SIZE / 5.8, GL_VIEW_SIZE / 10., this->camera->center.z);
		glScalef(border, border, 0.);
		glTranslatef(i * 0.35, 0., 0.);
		glColor3f(1., 0., 0.);
		drawHeart();
		glPopMatrix();
	}
}


void Scene::getBonus(){
	// apply existing bonus
	if(this->bonus->useBonus(GLUE)){
		this->ball->stick = true;
	}else{
		this->racket->resetColor();
		this->ball->stick = false;
	}

	int index = this->bonus->isHit(this->racket->position, this->racket->scale / 2.);
	if(index < 0 || this->bonus->bonus.at(index).applied)
		return;

	Bonus apply = this->bonus->bonus.at(index);

	switch (apply.id)
	{
		case LIFE:
			this->gainLife();
			break;
		case GLUE:
			this->ball->stick = true;
			this->racket->color = createColor(1., 0., 0.);
			break;	
		default:
			break;
	}

	this->bonus->desactivate(index);
}