#include "camera.h"

void Camera::set()
{
    gluLookAt(
        this->eye.x, this->eye.y, this->eye.z,
        this->center.x, this->center.y, this->center.z,
        this->up.x, this->up.y, this->up.z);
}

void Camera::move(Vector3D direction)
{
    this->eye = pointPlusVector(this->eye, direction);
    this->center = pointPlusVector(this->center, direction);
}