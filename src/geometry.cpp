#include <math.h>
#include <stdio.h>

#include "geometry.h"

Point3D createPoint(float x, float y, float z)
{
    Point3D newPoint;
    newPoint.x = x;
    newPoint.y = y;
    newPoint.z = z;

    return newPoint;
}

Vector3D createVector(float x, float y, float z)
{
    Vector3D newVector;
    newVector.x = x;
    newVector.y = y;
    newVector.z = z;

    return newVector;
}

Vector3D createVectorFromPoints(Point3D p1, Point3D p2)
{
    float x = p1.x - p2.x;
    float y = p1.y - p2.y;
    float z = p1.z - p2.z;
    Vector3D vectorFromPoints = createVector(x, y, z);

    return vectorFromPoints;
}

Point3D pointPlusVector(Point3D p, Vector3D v)
{
    float x = p.x + v.x;
    float y = p.y + v.y;
    float z = p.z + v.z;
    Point3D sumPointVector = createPoint(x, y, z);

    return sumPointVector;
}

Vector3D addVectors(Vector3D v1, Vector3D v2)
{
    float x = v1.x + v2.x;
    float y = v1.y + v2.y;
    float z = v1.z + v2.z;
    Vector3D sumVector = createVector(x, y, z);

    return sumVector;
}

Vector3D subVectors(Vector3D v1, Vector3D v2)
{
    float x = v1.x - v2.x;
    float y = v1.y - v2.y;
    float z = v1.z - v2.z;
    Vector3D subVector = createVector(x, y, z);

    return subVector;
}

Vector3D multVector(Vector3D v, float a)
{
    float x = a * v.x;
    float y = a * v.y;
    float z = a * v.z;
    Vector3D multVector = createVector(x, y, z);

    return multVector;
}

Vector3D divVector(Vector3D v, float a)
{
    float x = v.x / a;
    float y = v.y / a;
    float z = v.z / a;
    Vector3D divVector = createVector(x, y, z);

    return divVector;
}

float dot(Vector3D a, Vector3D b)
{
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}

float norm(Vector3D v)
{
    return sqrt(dot(v, v));
}

Vector3D normalize(Vector3D v)
{
    float x = v.x / norm(v);
    float y = v.y / norm(v);
    float z = v.z / norm(v);
    Vector3D normalizedVector = createVector(x, y, z);

    return normalizedVector;
}
