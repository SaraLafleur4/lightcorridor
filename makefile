CC 		= g++
CFLAGS 	= -w -Wall -Wextra
LDFLAGS	= -lglfw -lGLU -lGL -lm

BINDIR	= bin/
INCDIR	= inc/
SRCDIR	= src/
OBJDIR	= obj/

OBJ = $(OBJDIR)main.o $(OBJDIR)racket.o $(OBJDIR)ball.o \
$(OBJDIR)tools.o $(OBJDIR)color.o $(OBJDIR)geometry.o \
$(OBJDIR)texture.o $(OBJDIR)obstacle.o $(OBJDIR)bonus.o \
$(OBJDIR)scene.o $(OBJDIR)camera.o \
$(OBJDIR)menu.o
#write here "$(OBJDIR)FILENAME.o" to include new files

# create directory needed before the compilation
all :
	mkdir -p $(BINDIR)
	mkdir -p $(OBJDIR)
	make lightcorridor
	$(BINDIR)lightcorridor

lightcorridor : $(OBJ)
	$(CC) $^ -o $(BINDIR)$@ $(CFLAGS) $(LDFLAGS)
	@echo "---- compilation terminated ! ----"
	@echo "use $(BINDIR)$@ to execute the program"

$(OBJDIR)%.o: $(SRCDIR)%.cpp $(INCDIR)*.h
	mkdir -p `dirname $@`
	$(CC) -o $@ -I $(INCDIR) -c $< $(CFLAGS)

clean :
	rm -rf *~
	rm -rf $(SRCDIR)*/*~
	rm -rf $(OBJDIR)
	rm -rf $(BINDIR)*
	@echo "---- All clean ! ----"