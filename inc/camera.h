#ifndef CAMERA
#define CAMERA

#include <GLFW/glfw3.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include "geometry.h"

typedef struct camera
{
    Point3D eye, center;
    Vector3D up;

    camera()
    {
        this->eye = createPoint(0., 0., 2.);
        this->center = createPoint(0., 0., 0.);
        this->up = createVector(0., 1., 0.);
    }

    void set();
    void move(Vector3D direction);

    /* Camera parameters and functions */
    /*
    float theta = 45.0f;     // Angle between x axis and viewpoint
    float phy = 60.0f;       // Angle between z axis and viewpoint
    float dist_zoom = 30.0f; // Distance between origin and viewpoint
    */
} Camera;

#endif