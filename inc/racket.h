#ifndef RACKET
#define RACKET


#include "geometry.h"
#include "color.h"

typedef struct racket
{
    Point3D position;
    Vector3D direction, norm;
    ColorRGB color;
    ColorRGB defaultColor = createColor(1., 1., 1.);
    float scale;

    racket()
    {
        this->position = createPoint(0., 0., 0.);
        this->direction = createVector(0., 0., -0.1);
        this->norm = createVector(0., 0., -1.);
        this->color = createColor(1., 1., 1.);
        this->scale = 1 / 2.5;
    }

    /** @brief Draws the game's racket */
    void draw();
    void moveForward(int iterate = 1);
    void resetColor();
} Racket;

#endif