#ifndef MENU
#define MENU

#include <iostream>
using namespace std;

#include <GLFW/glfw3.h>

void displayMenu(GLuint image, float depth);
#endif