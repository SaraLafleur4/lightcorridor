#ifndef BALL
#define BALL

#include "geometry.h"
#include "color.h"

enum idRacketPosition
{
    TOP_LEFT_1, TOP_LEFT_2, TOP_LEFT_3, 
    TOP_RIGHT_1, TOP_RIGHT_2, TOP_RIGHT_3, 
    BOTTOM_LEFT_1, BOTTOM_LEFT_2, BOTTOM_LEFT_3, 
    BOTTOM_RIGHT_1, BOTTOM_RIGHT_2, BOTTOM_RIGHT_3,    
};
enum idWallPosition
{
    LEFT_WALL, RIGHT_WALL, TOP_WALL, BOTTOM_WALL
};

typedef struct ball
{
    Point3D position;
    Vector3D initialDirection, direction, rad;
    ColorRGB color;
    bool stick;
    int positionWithinRacket;
    int positionWhichWall;

    /* Setup new ball */
    ball()
    {
        this->position = createPoint(0., 0., -0.1);
        this->initialDirection = createVector(0., 0., -0.15);
        this->direction = createVector(0., 0., -0.15);
        this->rad = createVector(0.1, 0.1, 0.1);
        this->color = createColor(1., 0., 0.);
        this->stick = true;
        this->positionWithinRacket = -1;
    }

    /* Draws the game's ball */
    void draw();
    void drawShadow(float viewSize);
    void move();

    bool isInFront(Point3D coord);
    void reset(Point3D position, Vector3D direction);
    void wait(GLFWwindow *window, Point3D position, Vector3D direction);

    bool inRacket(Point3D racketPosition, float racketScale);
    bool inWall(Vector3D scene);

    void newDirection(Vector3D norm, float coef);
    void newDirectionRacket(Vector3D racketNorm);
    void newDirectionWall();
} Ball;

#endif