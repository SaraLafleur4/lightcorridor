#ifndef OBSTACLE
#define OBSTACLE

#include <iostream>
#include <vector>

#include <cstdlib>

#include "geometry.h"
#include "color.h"
#include "texture.h"

const int NB_SAMPLE_PANELS = 5;

enum idPanel
{
    RIGHT_PANEL,
    CENTER_PANEL,
    LEFT_PANEL,
    TOP_PANEL,
    BOTTOM_PANEL
};

typedef struct panel
{
    ColorRGB color;
    float depth;
    Vector3D norm;
    int id;

    panel() {}
    panel(float r, float g, float b, float depth)
    {
        this->color = createColor(r, g, b);
        this->depth = -depth; // always NEGATIVE
        this->norm = createVector(0., 0., 1.);
        this->id = rand() % NB_SAMPLE_PANELS;
    }

    void drawPanel(Vector3D scene, GLuint image);
    void draw(float position, float variation, Vector3D scene, GLuint image);
    bool inPanel(Point3D point, float border, Vector3D scene);
} Panel;

typedef struct lstPanel
{
    std::vector<Panel> panels;

    lstPanel() {}
    lstPanel(int nbPanels, float zMax)
    {
        float distance = 0.;
        // for (int i = 0; i < nbPanels; i++)
        while(distance < zMax)
        {
            float add = rand() % (int)(zMax - distance) + 3;
            this->panels.push_back(Panel(0., 0.9, 0.8, add));
            distance += add;
        }
    }

    void draw(float position, float variation, Vector3D scene, GLuint image);
} Panels;

#endif