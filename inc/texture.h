#ifndef TEXTURE
#define TEXTURE

#include <GLFW/glfw3.h>

GLuint loadImage(char *imageName, unsigned char *recup_image);

#endif