#ifndef SCENE
#define SCENE

#include <GLFW/glfw3.h>
#include "color.h"
#include "geometry.h"

#include "camera.h"
#include "obstacle.h"
#include "racket.h"
#include "ball.h"
#include "bonus.h"

typedef struct scene
{
    Panels *obstacles;
    LstBonus *bonus;
    Camera *camera = new Camera();
    Racket *racket = new Racket();
    Ball *ball = new Ball();
    
    const float border = 0.2;
    ColorRGB color = createColor(0., 0.6, 0.3);
    Vector3D wallDimensions = createVector(1.8, 1., 12.);
    
    int life;
    float lenght;

    scene(){
        this->life = 5;
        this->lenght = 30.;
        this->obstacles = new Panels(6, this->lenght);
        this->bonus = new LstBonus(6, this->lenght);
    }

    scene(float lenght, int life){
        this->lenght = lenght;
        this->life = life;
        this->obstacles = new Panels(6, this->lenght);
        this->bonus = new LstBonus(6, this->lenght);
    }

    /* Scene setup -> 4 walls of the corridor */
    void drawWall(Point3D position);
    void draw(GLuint texture);

    bool lostLife();
    bool gainLife();
    void drawLife();

    bool finished();

    void getBonus();
    void moveRacket(GLFWwindow *window); 
    bool racketIsBlocked();
    bool collisionWithObstacle();
    bool collisionWithRacket();
    bool collisionWithWall();
} Scene;

void drawLeftWall(float x, float y, float z);
void drawRightWall(float x, float y, float z);
void drawTopWall(float x, float y, float z);
void drawBottomWall(float x, float y, float z);

#endif