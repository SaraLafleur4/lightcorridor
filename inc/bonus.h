#ifndef BONUS
#define BONUS

#include <iostream>
#include <vector>
#include <cstdlib>
#include "geometry.h"
#include "tools.h"

#define NB_BONUS 2

enum idBonus{
    LIFE, GLUE
};

typedef struct bonus
{
    coords3D position;
    coords3D minSize;
    coords3D maxSize;
    int id;
    float time;
    float angle;
    bool applied;
    
    bonus(){}

    bonus(coords3D position, float time){
        this->position = position;
        this->id = rand() % NB_BONUS;
        switch(id)
        {
        case LIFE:
            this->minSize = createPoint(0.2, 0.15, 0.);
            this->maxSize = createPoint(0.2, 0.2, 0.);
            break;
        
        case GLUE:
            this->minSize = createPoint(0.2, 0.15, 0.);
            this->maxSize = createPoint(0.2, 0.2, 0.);
            break;
        default:
            break;
        }
        this->time = time;
        this->angle = 0.;
        this->applied = false;
    }

    bool isHit(coords3D position, float size);
    void draw();
    bool stillActive();
} Bonus;

typedef struct lstBonus
{
    std::vector<Bonus> bonus;
    
    lstBonus(){}

    lstBonus(int nbBonus, float lenght){
        coords3D position = createPoint(0, 0, -5);
        for(int i = 0; i < nbBonus; i++){
            int id = rand() % (NB_BONUS + 1);
            float time;
            coords3D add;

            switch (id)
            {
            case LIFE: time = 0; break;
            case GLUE: time = 10.; break;
            default: break;
            }

            add = createPoint(
                ((rand() % (int)GL_VIEW_SIZE) - GL_VIEW_SIZE / 2.) / GL_VIEW_SIZE, 
                ((rand() % (int)GL_VIEW_SIZE) - GL_VIEW_SIZE / 2.) / GL_VIEW_SIZE,  
                - rand() % (int)(lenght + position.z + lenght / 2)
            );
            this->bonus.push_back(Bonus(add, time));

            position = addVectors(position, add);
        }
    }
    void draw();
    int isHit(coords3D position, float border);
    void desactivate(int index);
    bool useBonus(idBonus id);
    void resetActive();
} LstBonus;

#endif