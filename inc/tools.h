#ifndef TOOLS
#define TOOLS

#include <GLFW/glfw3.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define NB_SEG_CIRCLE 64

/* Window properties */
static const unsigned int WINDOW_WIDTH = 1280;
static const unsigned int WINDOW_HEIGHT = 800;
static const char WINDOW_TITLE[] = "The Light Corridor";
static float aspectRatio = 1.;

/* Minimal time wanted between two images */
static const double FRAMERATE_IN_SECONDS = 1. / 30.;

/* Virtual window's space */
// Space is defined in interval -1 and 1 on x and y axes
static const float GL_VIEW_SIZE = 10.;

/* Camera parameters */
static const float Z_NEAR = 0.1f;
static const float Z_FAR = 100.f;

/* Small tools */
float toRad(float deg);
bool isBetween(float compare, float border, float smallest, float biggest);

void onError(int error, const char *description);
void onWindowResized(GLFWwindow *window, int width, int height);
void onKey(GLFWwindow *window, int key, int scancode, int action, int mods);

void drawFrame();
// Canonic object
void drawCircle();
void drawSphere();
void drawCone();
void drawHeart();
void drawSquare();

#endif