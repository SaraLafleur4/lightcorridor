#ifndef COLOR
#define COLOR

typedef struct color
{
    float r, g, b;
}ColorRGB;

ColorRGB createColor(float r, float g, float b);

#endif